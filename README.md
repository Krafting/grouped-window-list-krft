*Forked from [here](https://github.com/linuxmint/cinnamon/tree/master/files/usr/share/cinnamon/applets/grouped-window-list%40cinnamon.org)*

# grouped-window-list@krafting.net
A not-so crappy anymore fork of GWL on cinnamon, it enable "Show all windows from all monitor" by default (might have broken the workspace thing tho)

# Should I use it ?
It shouldn't give you any headache; if it does; just open an issue here.

# Known issues ?
No known issues for the moment, feel free to open issues here if you find one!

Will probably break, don't know yet

# How to install ? 

This method should work:
```
    git clone https://github.com/Krafting/grouped-window-list-krft.git
    cd grouped-window-list-krft
    mv grouped-window-list@krafting.net ~/.local/share/cinnamon/applets/
```
 

 You can also use this method:

```
    git clone https://github.com/Krafting/grouped-window-list-krft.git
    cd grouped-window-list-krft
    sudo mv grouped-window-list@krafting.net /usr/share/cinnamon/applets/
```

Then, just add the applets on your panel via the Applets App.

# Contributing ?
yes please, help me
